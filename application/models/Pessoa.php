<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pessoa extends CI_Model{
    
    public function Pessoa(){
        parent::__construct();
    //  $this->load->database();
    }
    
    public function alterar($dados){
        $vetor = array(
            'nome' => $dados['nome'],
            'cidade' => $dados['cidade']);
        
        $this->db->where('id',$dados['id']);
        $this->db->update('pessoa',$dados);        
    }
    
    public function cadastrar($pessoa){
    //  $query = $this->db->query("insert into pessoa (nome) values ('".$pessoa."');");           
        $vetor = array(
            'nome' => $pessoa['nome'],
            'cidade' => $pessoa['cidade'] );
        
        $this->db->insert('pessoa',$vetor);        
    }
    
    public function consultarTodos(){
    //  $query = $this->db->query('select * from pessoa;');
        $query = $this->db->get('pessoa');
        return $query->result();
    }
    
    public function excluir($id){
        $this->db->query("delete from pessoa where id = ".$id.";"); 
    }
    
    public function buscaPessoa($id){
    //    $query = $this->db->query("select * from pessoa where id = ". $id .";");
        $this->db->where('id',$id);
        return $this->db->get('pessoa')->row();         
    }
}
 

?>

<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <center>
            <h3>CADASTRO</h3></br>
            <?php
            
                $nome   = array('type'  => 'text', 'name'  => 'nome');                
                $cidade = array('type'  => 'text', 'name'  => 'cidade');
            
                echo form_open('control/adicionar/');
                    echo form_label('Nome','nome');
                    echo form_input($nome).'<br/><br/>';
                    
                    echo form_label('Cidade','cidade');
                    echo form_input($cidade);
                    
                    echo form_submit('enviar','Enviar');
                echo form_close();
            ?>
            <br/><br/>
            <a href="<?php echo base_url('index.php/control/'); ?>">VOLTAR</a>
        </center>
    </body>
</html>

<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Aula Teste</title>
</head>
    <body>       
        <center>
            <table border="1" width="30%">
                <caption><a href=" <?php echo base_url('index.php/control/paginaCadastro/');?> ">Adicionar Pessoa</a></caption>
                <thead>
                    <th>ID</th><th>Nome</th><th>Cidade</th><th>Alterar</th><th>Excluir</th>
                </thead> 
                <tbody>      
                    <?php     
                        foreach($dados as $pessoa){
                            echo '<tr align="center">';
                            echo    '<td>'.$pessoa->id.'</td>';
                            echo    '<td>'.$pessoa->nome.'</td>';
                            echo    '<td>'.$pessoa->cidade.'</td>';
                            echo    '<td><a href="'. base_url('index.php/control/retornaPessoa/'.$pessoa->id).'">alt</a></td>';                        
                            echo    '<td><a href="'. base_url('index.php/control/excluirPessoa/'.$pessoa->id).'">del</a></td>';
                            echo '</tr>';
                        }    
                    ?>  
                </tbody>       
            </table>     
        </center> 
    </body>
</html>
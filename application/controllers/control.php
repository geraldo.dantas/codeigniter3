<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Control extends CI_Controller {

        public function __construct(){
            parent::__construct();            
        }        
        
	public function index()	{   
            $this->retornaTodos();            
	}
                        
        public function adicionar(){ 
            $this->load->model('pessoa');
            
            $info['nome'] = $this->input->post('nome');   
            $info['cidade'] = $this->input->post('cidade');           
            $resultado = $this->pessoa->cadastrar($info); 
           
            $this->retornaTodos();
        }
        
                
        public function alterarPessoa(){  
            $this->load->helper('form');
            $this->load->model('pessoa');
            
            $dados['id'] = $this->input->post('id');
            $dados['nome'] = $this->input->post('nome');
            $dados['cidade'] = $this->input->post('cidade');
            $this->pessoa->alterar($dados);
            
            $this->retornaTodos();
        }
        
        public function excluirPessoa($id){      
            $this->load->model('pessoa');
            $this->pessoa->excluir($id);
            $this->retornaTodos();
        }
        
        public function paginaCadastro(){ 
            $this->load->view('cadastro');            
        }
        
        public function retornaPessoa($id){
            $this->load->model('pessoa');                      
            $info['dados'] = $this->pessoa->buscaPessoa($id); 
            $this->load->view('alteracao',$info);
        }
        
        public function retornaTodos(){   
            $this->load->model('pessoa');
            $vetor['dados'] = $this->pessoa->consultarTodos(); 
            $this->load->view('tela', $vetor);
        }                
}